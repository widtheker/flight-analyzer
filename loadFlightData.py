import pyspark
from pyspark.sql import SparkSession

"""
Prereqs:
    Goal (G):
        ad. Predict arrival delay
    TimeOfUsage (T):
        bs. Before scheduled takeoff
        as. After scheduled takeoff
        aa. After actual takeoff
        x. Any
    Model (M):
        b. Basic
        oa. IncludeOriginAirport
        da. IncludeDestinationAirport
        tn. IncludeTailnumberSituation
"""



""" Name	Description
1	Year	1987-2008
2	Month	1-12
3	DayofMonth	1-31
4	DayOfWeek	1 (Monday) - 7 (Sunday)
5	DepTime	actual departure time (local, hhmm)
6	CRSDepTime	scheduled departure time (local, hhmm)
7	ArrTime	actual arrival time (local, hhmm)
8	CRSArrTime	scheduled arrival time (local, hhmm)
9	UniqueCarrier	unique carrier code
10	FlightNum	flight number
11	TailNum	plane tail number
12	ActualElapsedTime	in minutes
13	CRSElapsedTime	in minutes
14	AirTime	in minutes
15	ArrDelay	arrival delay, in minutes
16	DepDelay	departure delay, in minutes
17	Origin	origin IATA airport code
18	Dest	destination IATA airport code
19	Distance	in miles
20	TaxiIn	taxi in time, in minutes
21	TaxiOut	taxi out time in minutes
22	Cancelled	was the flight cancelled?
23	CancellationCode	reason for cancellation (A = carrier, B = weather, C = NAS, D = security)
24	Diverted	1 = yes, 0 = no
25	CarrierDelay	in minutes
26	WeatherDelay	in minutes
27	NASDelay	in minutes
28	SecurityDelay	in minutes
29	LateAircraftDelay	in minutes """


# comments on data
# 17, 18, 19 contain redundant info, however in case I would skip the distance
# and the data is limited for some airports I think I would lose information 


# Select indices
selectedIndices_TbsMb = [2,3,4,6,8,9,10,11,17,18,19]

# Years covered by the data
#years = list(range(1987,2009))

# Load data
# The files are stored in the following format
# "/windows-downloads/1987.csv.bz2" where year differs from 1987 to 2008
dataDirectory = "/windows-downloads/"
filenameEnding = ".csv.bz2"
warehouse_location = "/home/osboxes/spark-sql-warehouse"

#fileNames = list(map(lambda year: dataDirectory + str(year) + filenameEnding,years))
spark = SparkSession \
    .builder \
    .config("spark.sql.warehouse.dir", warehouse_location) \
    .appName("Flight Delay Analyzer") \
    .getOrCreate()


df = spark.read.load(dataDirectory + "*" + filenameEnding,format="csv", sep=",", inferSchema="true", header="true")

# Not lazy - takes time.
# https://spark.apache.org/docs/latest/sql-data-sources-load-save-functions.html
# https://github.com/apache/spark/blob/master/examples/src/main/python/sql/datasource.py



df.printSchema()
'''root
 |-- Year: integer (nullable = true)
 |-- Month: integer (nullable = true)
 |-- DayofMonth: integer (nullable = true)
 |-- DayOfWeek: integer (nullable = true)
 |-- DepTime: string (nullable = true)
 |-- CRSDepTime: integer (nullable = true)
 |-- ArrTime: string (nullable = true)
 |-- CRSArrTime: integer (nullable = true)
 |-- UniqueCarrier: string (nullable = true)
 |-- FlightNum: integer (nullable = true)
 |-- TailNum: string (nullable = true)
 |-- ActualElapsedTime: string (nullable = true)
 |-- CRSElapsedTime: string (nullable = true)
 |-- AirTime: string (nullable = true)
 |-- ArrDelay: string (nullable = true)
 |-- DepDelay: string (nullable = true)
 |-- Origin: string (nullable = true)
 |-- Dest: string (nullable = true)
 |-- Distance: string (nullable = true)
 |-- TaxiIn: string (nullable = true)
 |-- TaxiOut: string (nullable = true)
 |-- Cancelled: integer (nullable = true)
 |-- CancellationCode: string (nullable = true)
 |-- Diverted: integer (nullable = true)
 |-- CarrierDelay: string (nullable = true)
 |-- WeatherDelay: string (nullable = true)
 |-- NASDelay: string (nullable = true)
 |-- SecurityDelay: string (nullable = true)
 |-- LateAircraftDelay: string (nullable = true)'''

df.count()
# 123534969

df.first()
# Row(Year=2007, Month=1, DayofMonth=1, DayOfWeek=1, DepTime='1232', CRSDepTime=1225, ArrTime='1341', CRSArrTime=1340, UniqueCarrier='WN', FlightNum=2891, TailNum='N351', ActualElapsedTime='69', CRSElapsedTime='75', AirTime='54', ArrDelay='1', DepDelay='7', Origin='SMF', Dest='ONT', Distance='389', TaxiIn='4', TaxiOut='11', Cancelled=0, CancellationCode=None, Diverted=0, CarrierDelay='0', WeatherDelay='0', NASDelay='0', SecurityDelay='0', LateAircraftDelay='0')

# this may improve performance?
df.cache()


# Panda is a popular 
import numpy as np
import pandas as pd
# If you take too much data you get memory problems
# Possible explanation: https://stackoverflow.com/questions/47536123/collect-or-topandas-on-a-large-dataframe-in-pyspark-emr
#result_pdf = df.select("*").toPandas()
# https://towardsdatascience.com/machine-learning-with-pyspark-and-mllib-solving-a-binary-classification-problem-96396065d2aa
# nice graphics
pd.DataFrame(df.take(5), columns=df.columns).transpose()
sampleFraction = 50/123534969 # or 2000
# the following was quick for some reason
df2 = df.sample(fraction=sampleFraction)

# quick
df2.cache()


# for some reason I dont get it to work passing df2 directly
pd.DataFrame(df2.take(100), columns=df2.columns).transpose()
# pretty many columns have 

type(df2)
# this shows pyspark.sql.dataframe.DataFrame

# an alternative with more control: df2.summary()
# df2.describe()

# count number of nulls in (dont understan, wait with this)
df2.filter(condition=df2.AirTime.isNull()).count()

df3= df2.select(df2.Year)

pd.DataFrame(df3.take(100), columns=df3.columns)

#df3=df2.select(df2.DayOfWeek,df2.ArrDelay)
df3=df2.withColumn('DayOfWeek',df2.DayOfWeek).withColumn('ArrDelay',int(df2('ArrDelay'))).collect()
pd.DataFrame(df3.take(100), columns=df3.columns)
df4 = df3.na.drop()

df3.corr('DayOfWeek','ArrDelay')


# TODO: try scala instead for better IDE support


# handling of the nominal values (categories)
# Discusses different feature extraction types: https://medium.com/vickdata/four-feature-types-and-how-to-transform-them-for-machine-learning-8693e1c24e80
# Good full example, including descrete, limited series: https://towardsdatascience.com/machine-learning-with-pyspark-and-mllib-solving-a-binary-classification-problem-96396065d2aa
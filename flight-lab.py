import pyspark
from pyspark.sql import SparkSession

dataDirectory = "/windows-downloads/"
filenameEnding = ".csv.bz2"
warehouse_location = "/home/osboxes/spark-sql-warehouse"

spark = SparkSession \
    .builder \
    .config("spark.sql.warehouse.dir", warehouse_location) \
    .appName("Flight Delay Analyzer") \
    .getOrCreate()

df = spark.read.load(dataDirectory + "*" + filenameEnding,format="csv", sep=",", inferSchema="true", header="true")

